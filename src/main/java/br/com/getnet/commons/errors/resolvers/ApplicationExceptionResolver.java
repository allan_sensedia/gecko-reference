package br.com.getnet.commons.errors.resolvers;

import br.com.getnet.commons.errors.domains.DefaultErrorResponse;
import br.com.getnet.commons.errors.exceptions.ApplicationException;
import org.springframework.stereotype.Service;

@Service
public class ApplicationExceptionResolver implements Resolver<ApplicationException> {

  @Override
  public DefaultErrorResponse getErrorResponse(ApplicationException e) {
    return e.getDefaultErrorResponse();
  }
}
