package br.com.getnet.commons.errors.resolvers;

import br.com.getnet.commons.errors.domains.DefaultErrorResponse;

public interface Resolver<T extends Throwable> {
  DefaultErrorResponse getErrorResponse(T e);
}
