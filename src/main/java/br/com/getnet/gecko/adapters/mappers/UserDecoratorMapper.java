package br.com.getnet.gecko.adapters.mappers;

import br.com.getnet.gecko.adapters.dtos.UserUpdateDto;
import br.com.getnet.gecko.domains.User;
import br.com.getnet.gecko.domains.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class UserDecoratorMapper implements UserMapper {

  @Autowired
  @Qualifier("delegate")
  private UserMapper delegate;

  @Override
  public User toUser(UserUpdateDto userUpdateDto) {
    validateStatus(userUpdateDto);
    return delegate.toUser(userUpdateDto);
  }

  private void validateStatus(UserUpdateDto userUpdateDto) {
    UserStatus.fromValue(userUpdateDto.getStatus());
  }
}
