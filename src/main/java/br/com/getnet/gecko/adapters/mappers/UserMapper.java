package br.com.getnet.gecko.adapters.mappers;

import br.com.getnet.gecko.adapters.dtos.UserCreationDto;
import br.com.getnet.gecko.adapters.dtos.UserDto;
import br.com.getnet.gecko.adapters.dtos.UserUpdateDto;
import br.com.getnet.gecko.domains.User;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(UserDecoratorMapper.class)
public interface UserMapper {
  User toUser(UserCreationDto userCreationDto);

  User toUser(UserUpdateDto userUpdateDto);

  UserDto toUserDto(User user);

  List<UserDto> toUserDtos(List<User> users);
}
