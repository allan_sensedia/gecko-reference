package br.com.getnet.gecko.adapters.repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import br.com.getnet.commons.errors.exceptions.PreConditionException;
import br.com.getnet.gecko.domains.User;
import br.com.getnet.gecko.domains.search.UserSearch;
import br.com.getnet.gecko.domains.search.UserSearchResponse;

public class AdvancedUserSearchImpl implements AdvancedUserSearch {

  @Value("${app.repository.maximumLimit}")
  private int maximumLimit;

  @Value("${app.repository.defaultLimit}")
  private int defaultLimit;
  
  @PersistenceContext
  private EntityManager em;

  @Override
  public UserSearchResponse findAll(UserSearch userSearch) {
    int limit = userSearch.getLimit() == null ? defaultLimit : userSearch.getLimit();

    if (limit > maximumLimit) {
      throw new PreConditionException(
          "The 'limit' field is greater than the configured maximum limit [" + maximumLimit + "]");
    }
    
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
    Root<User> user = criteriaQuery.from(User.class);
    
  //Cria restrições
  	Predicate[] predicates = criarRestricoes(userSearch, cb, user);
  	criteriaQuery.where(predicates);
  	
  	TypedQuery<User> query = em.createQuery(criteriaQuery);

    int page = userSearch.getPage() > 0 ? userSearch.getPage() - 1 : 0;

    Sort.Direction direction = Sort.Direction.fromString(userSearch.getSortType().getValue());

    PageRequest.of(page, limit, Sort.by(direction, userSearch.getSort().getFieldName()));

    return new UserSearchResponse(query.getResultList(), total(userSearch).intValue(), maximumLimit);
  }

  private Predicate[] criarRestricoes(UserSearch userSearch, CriteriaBuilder cb, Root<User> user) {
	    
	    List<Predicate> predicates = new ArrayList<>();
	    
	    if (StringUtils.isNotBlank(userSearch.getName())) {
	        predicates.add(cb.equal(user.get("name"), userSearch.getName()));
	    }

	    if (StringUtils.isNotBlank(userSearch.getEmail())) {
	        predicates.add(cb.equal(user.get("name"), userSearch.getEmail()));
	    }
	    
	    if(java.util.Objects.nonNull(userSearch.getStatus())) {
	    	predicates.add(cb.equal(user.get("status"), userSearch.getStatus()));
	    }

	    if (userSearch.getCreatedAtStart() != null) {
	    	Path<Instant> createdAtStart = user.get("createdAtStart");
	        predicates.add(
	                cb.greaterThanOrEqualTo(createdAtStart, userSearch.getCreatedAtStart()));
	    }

	    if (userSearch.getCreatedAtEnd() != null) {
	    	Path<Instant> createdAtEnd = user.get("createdAtEnd");
	        predicates.add(
	                cb.lessThanOrEqualTo(createdAtEnd, userSearch.getCreatedAtEnd()));
	    }
		return predicates.toArray(new Predicate[0]);
  }
  

	private Long total(UserSearch userSearch) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<User> user = criteria.from(User.class);
		
		Predicate[] predicates = criarRestricoes(userSearch, builder, user);
		criteria.where(predicates);
		
		criteria.select(builder.count(user));
		return em.createQuery(criteria).getSingleResult();
	}
}
