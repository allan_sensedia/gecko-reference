package br.com.getnet.gecko.adapters.repository;

import br.com.getnet.gecko.domains.search.UserSearch;
import br.com.getnet.gecko.domains.search.UserSearchResponse;

public interface AdvancedUserSearch {

  UserSearchResponse findAll(UserSearch userSearch);
}
