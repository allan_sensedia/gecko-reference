package br.com.getnet.gecko.adapters.amqp;

import br.com.getnet.commons.beans.BeanValidator;
import br.com.getnet.commons.errors.resolvers.ExceptionResolver;
import br.com.getnet.gecko.adapters.amqp.config.BindConfig;
import br.com.getnet.gecko.adapters.amqp.config.BrokerInput;
import br.com.getnet.gecko.adapters.dtos.UserCreationDto;
import br.com.getnet.gecko.adapters.dtos.UserDeletionDto;
import br.com.getnet.gecko.adapters.dtos.UserUpdateDto;
import br.com.getnet.gecko.adapters.mappers.UserMapper;
import br.com.getnet.gecko.domains.User;
import br.com.getnet.gecko.ports.AmqpPort;
import br.com.getnet.gecko.ports.ApplicationPort;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(BrokerInput.class)
public class AmqpUserAdapterInbound {

  private final ApplicationPort applicationPort;
  private final UserMapper userMapper;
  private final AmqpPort amqpPort;
  private final ExceptionResolver exceptionResolver;

  public AmqpUserAdapterInbound(
      ApplicationPort applicationPort,
      UserMapper userMapper,
      AmqpPort amqpPort,
      ExceptionResolver exceptionResolver) {
    this.applicationPort = applicationPort;
    this.userMapper = userMapper;
    this.amqpPort = amqpPort;
    this.exceptionResolver = exceptionResolver;
  }

  @StreamListener(target = BindConfig.SUBSCRIBE_USER_CREATION_REQUESTED)
  public void subscribeExchangeUserCreationRequested(UserCreationDto userCreationDto) {
    try {
      User user = userMapper.toUser(userCreationDto);
      applicationPort.create(user);
    } catch (Exception e) {
      amqpPort.notifyUserOperationError(
          exceptionResolver.solve(e).addOriginalMessage(userCreationDto));
    }
  }

  @StreamListener(target = BindConfig.SUBSCRIBE_USER_DELETION_REQUESTED)
  public void subscribeExchangeUserDeletionRequested(UserDeletionDto userDeletionDto) {
    try {
      applicationPort.delete(userDeletionDto.getId());
    } catch (Exception e) {
      amqpPort.notifyUserOperationError(
          exceptionResolver.solve(e).addOriginalMessage(userDeletionDto));
    }
  }

  @StreamListener(target = BindConfig.SUBSCRIBE_USER_UPDATE_REQUESTED)
  public void subscribeExchangeUserUpdateRequested(UserUpdateDto userUpdateDto) {
    try {
      BeanValidator.validate(userUpdateDto);

      User user = userMapper.toUser(userUpdateDto);

      applicationPort.update(user, userUpdateDto.getId());
    } catch (Exception e) {
      amqpPort.notifyUserOperationError(
          exceptionResolver.solve(e).addOriginalMessage(userUpdateDto));
    }
  }
}
