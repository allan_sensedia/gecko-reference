package br.com.getnet.gecko.ports;

import br.com.getnet.gecko.domains.User;
import br.com.getnet.gecko.domains.search.UserSearch;
import br.com.getnet.gecko.domains.search.UserSearchResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ApplicationPort {
  User create(@Valid @NotNull User user);

  void delete(@NotNull String id);

  User update(@Valid @NotNull User user, @NotNull String id);

  User findById(@NotNull String id);

  UserSearchResponse findAll(@Valid @NotNull UserSearch userSearch);
}
