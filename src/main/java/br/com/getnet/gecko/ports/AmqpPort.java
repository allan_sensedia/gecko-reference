package br.com.getnet.gecko.ports;

import br.com.getnet.commons.errors.domains.DefaultErrorResponse;
import br.com.getnet.gecko.domains.User;

public interface AmqpPort {

  void notifyUserCreation(User user);

  void notifyUserDeletion(User user);

  void notifyUserOperationError(DefaultErrorResponse errorResponse);

  void notifyUserUpdate(User user);
}
