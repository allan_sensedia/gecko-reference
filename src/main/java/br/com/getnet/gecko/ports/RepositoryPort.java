package br.com.getnet.gecko.ports;

import br.com.getnet.gecko.adapters.repository.AdvancedUserSearch;
import br.com.getnet.gecko.domains.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryPort extends CrudRepository<User, String>, AdvancedUserSearch {}
