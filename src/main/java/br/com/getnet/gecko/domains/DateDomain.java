package br.com.getnet.gecko.domains;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
public abstract class DateDomain {
	
	@CreationTimestamp
	@Column(name="created", updatable = false)
	protected Instant createdAt;
	
	@UpdateTimestamp
	@Column(name="updated", insertable = false)
	protected Instant updatedAt;
	
}
