package br.com.getnet.gecko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "br.com.getnet")
public class App {

  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }
}
