package br.com.getnet.gecko.applications;

import br.com.getnet.commons.errors.exceptions.NotFoundException;
import br.com.getnet.gecko.domains.User;
import br.com.getnet.gecko.domains.UserStatus;
import br.com.getnet.gecko.domains.search.UserSearch;
import br.com.getnet.gecko.domains.search.UserSearchResponse;
import br.com.getnet.gecko.ports.AmqpPort;
import br.com.getnet.gecko.ports.ApplicationPort;
import br.com.getnet.gecko.ports.RepositoryPort;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@Service
@Transactional
public class UserApplication implements ApplicationPort {

    private final AmqpPort amqpPort;
    private final RepositoryPort repository;

    public UserApplication(AmqpPort amqpPort, RepositoryPort repository) {
        this.amqpPort = amqpPort;
        this.repository = repository;
    }

    @Override
    public User create(@Valid @NotNull User user) {
        user.setStatus(UserStatus.ACTIVE);

        repository.save(user);
        amqpPort.notifyUserCreation(user);

        return user;
    }

    @Override
    public void delete(@NotNull String id) {
        User user = findById(id);

        repository.delete(user);
        amqpPort.notifyUserDeletion(user);
    }

    @Override
    public User update(@Valid @NotNull User userForUpdate, @NotNull String id) {
        User user = findById(id);

        BeanUtils.copyProperties(userForUpdate, user, "id");

        repository.save(user);

        amqpPort.notifyUserUpdate(user);

        return user;
    }

    @Override
    public User findById(@NotNull String id) {
        return repository
                .findById(id)
                .orElseGet(
                        () -> {
                            throw new NotFoundException("User not found");
                        });
    }

    @Override
    public UserSearchResponse findAll(@Valid @NotNull UserSearch userSearch) {
        return repository.findAll(userSearch);
    }

}
