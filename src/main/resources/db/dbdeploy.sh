#!/bin/bash
driver="33"
url="33"
username="33"
password="33"
environment="33"

getProperties(){
 driver=com.mysql.cj.jdbc.Driver
 url="jdbc:mysql:\/\/${DATABASE_HOST:-localhost}:${DATABASE_PORT:-3306}\/api_gecko?useSSL=false"
 username=${DATABASE_USER_NAME:-api_gecko}
 password=${DATABASE_USER_PASSWORD:-api_gecko}
}

setPomDbDeploy() {
  /bin/cp pom-unparsed.xml pom.xml
  pom=pom.xml
  sed -i -e "s/\${spring.database.driverClassName}/$driver/g" $pom
  sed -i -e "s/\${spring.datasource.url}/$url/g" $pom
  sed -i -e "s/\${spring.datasource.username}/$username/g" $pom
  sed -i -e "s/\${spring.datasource.password}/$password/g" $pom
  sed -i -e "s/\${scriptEnvironment}/$environment/g" $pom
}
runDbDeploy() {
  if [[ $environment != $localEnv ]]; then
    mvn clean test dbdeploy:update
    /bin/rm -R ./temp
  fi
}
debug() {
  echo "driver $driver"
  echo "url $url"
  echo "username $username"
  echo "password  $password"
}
setScripts(){
  /bin/mkdir temp > /dev/null 2>&1
  /bin/cp ./scripts/"$environment"/* ./temp > /dev/null 2>&1
  /bin/cp ./*.sql ./temp > /dev/null 2>&1
}
getEnviroment(){
  env=['dev','stage','hml','prd']
  environment="dev"
  localEnv="local"
  if [ $# -eq 1 ]; then
    if echo "${env[*]}" | grep -q -w "$1"; then
      environment=$1
      localEnv=$1
    fi
  fi
  echo "dbdeploy running in environment: $environment"
}
main() {
  getEnviroment $1
  setScripts
  getProperties
  setPomDbDeploy
  runDbDeploy
}
main $*
